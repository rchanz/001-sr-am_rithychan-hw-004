import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        //Initialize custom collection
        CustomList<Integer> cl = new CustomList<>();
        System.out.println("");
        try {
//            for (int j = 0; j < cl.size(); j++) {
//                cl.add(j);
//            }
            cl.add(1);
            cl.add(null);
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("\nOutput:");
        for (int i = 0; i<cl.size(); i++) {
            System.out.println(cl.get(i));
        }
    }
}
