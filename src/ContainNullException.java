public class ContainNullException extends Exception {
    public ContainNullException (String message) {
        super(message);
    }
}
