import java.util.ArrayList;
import java.util.Arrays;

public class CustomList<E> {
    //Size of list
    private int size = 0;

    //Initial capacity
    private static final int DEFAULT_CAPACITY = 10;

    //Array to store elements then add to list
    private Object elements[];

    //Default constructor
    public CustomList() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    //Add method
    public void add(E e) throws Exception {
        if (size == elements.length) {
            ensureCapacity();
        }

        // Traverse through the first list
        for (int i = 0; i < elements.length; i++) {
            if (e != null || elements[i].equals(null)) {
                if (elements[i] != e) {
                    elements[size++] = e;
                    break;
                } else throw new ContainDuplicateException("Cannot contain duplicate value!");
            } else throw new NumberFormatException("Cannot contain null elements!");
        }
    }

    //Get method
    public E get(int i) {
        if (i >= size || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size " + i);
        }
        return (E) elements[i];
    }

    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    //Get Size of list
    public int size() {
        return size;
    }

    //Dynamic scaling
    private void ensureCapacity() {
        int newSize = elements.length * 2;
        elements = Arrays.copyOf(elements, newSize);
    }

    public int indexOf(Object o) {
        return indexOfRange(o, 0, size);
    }

    int indexOfRange(Object o, int start, int end) {
        Object[] es = elements;
        if (o == null) {
            for (int i = start; i < end; i++) {
                if (es[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = start; i < end; i++) {
                if (o.equals(es[i])) {
                    return i;
                }
            }
        }
        return -1;
    }
}
