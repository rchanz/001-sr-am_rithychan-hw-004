public class ContainDuplicateException extends Exception {
    public ContainDuplicateException (String message) {
        super(message);
    }
}
